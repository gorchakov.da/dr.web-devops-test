"""Module providing a start of app on TCP/8085 port."""

import argparse

from aiohttp import web

from package import create_application


def main(args):
    """Main function run web app."""
    app = create_application()
    logger = app.logger
    logger.info("Create application")
    try:
        web.run_app(app, port=args.port)
    finally:
        logger.info("Shutdown application")


def parse_args() -> argparse.Namespace:
    """Parse args function."""
    parser = argparse.ArgumentParser(prog="Test Package")
    default_application_port = 8085
    parser.add_argument(
        "-p", "--port", type=int, default=default_application_port,
        help=f"Application port (default: {default_application_port})",
    )
    return parser.parse_args()


if __name__ == "__main__":
    main(parse_args())
